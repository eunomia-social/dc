import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Cascade from '../components/cascade';
import { setCascadeViewOpen } from '../actions';

const mapStateToProps = state => {
  const statusId = state.getIn(['eunomia', 'cascadeOpen'], null);
  let eunomia = null;
  let cascadeId = null;
  let processed = false;
  if (statusId) {
    eunomia = state.getIn(['eunomia', statusId], null);
    if (eunomia) {
      cascadeId = eunomia.get('cascade_id', null);
      processed = eunomia.get('processed', 'no') === 'yes';
    }
  }
  return  {
    eunomia,
    cascadePosts: processed && cascadeId ? state.getIn(['cascades', cascadeId], null) : null,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onSetCascade(open) {
    dispatch(setCascadeViewOpen(open));
  },
  dispatch,
});

export default @connect(mapStateToProps, mapDispatchToProps)
class CascadePanel extends React.PureComponent {

  constructor(props) {
    super(props);
    this.onCloseCascade = this.onCloseCascade.bind(this);
  }

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    dispatch: PropTypes.func,
    eunomia: ImmutablePropTypes.map,
    cascadePosts: ImmutablePropTypes.list,
    onSetCascade: PropTypes.func.isRequired,
  };


  onCloseCascade() {
    this.props.onSetCascade(null);
  }

  render() {
    if (!this.props.eunomia || !this.props.cascadePosts) {
      this.context.router.history.goBack();
      return null;
    }
    return (
      <div className='compose-panel' style={{ width: '100%' }}>
        <Cascade onClose={this.onCloseCascade} eunomia={this.props.eunomia} posts={this.props.cascadePosts} />
      </div>
    );
  }

}
