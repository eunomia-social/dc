#!/bin/sh
# shellcheck disable=SC2068,SC2086

set -e

DC_IMAGE="${DC_IMAGE:-a.docker.registry/image}"
MASTODON_VERSION=${MASTODON_VERSION:-3.4.1}
MASTODON_CHECKOUT="v${MASTODON_VERSION}"
INTEGRATED_COMPOSE_FILE="${INTEGRATED_COMPOSE_FILE:-}"

_CWD="$(pwd)"
_HERE="$(dirname "$(readlink -f "$0")")"
if [ "${_HERE}" = "." ]; then
  _HERE="$(pwd)"
fi
cd "${_HERE}" || exit

loop_array() {
  for i; do printf %s\\n "$i" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/' \\\\/"; done
  echo " "
}

int() {
  printf '%d' ${1:-} 2>/dev/null || :
}

build_version() {
  _today="$(date +%Y%m%d)"
  _version="${_today}r1"
  _existing="$(docker images ${DC_IMAGE} | grep "${_today}" | tail -1 | awk '{print $2}' | cut -dr -f2 || echo '')"
  if [ ! "${_existing}" = "" ]; then
    _next="$(($(int "${_existing}") + 1))"
    _version="${_today}r${_next}"
  fi
  echo "${_version}"
}

EUNOMIA_VERSION="${EUNOMIA_VERSION:-$(build_version)}"
DC_VERSION="${DC_VERSION:-${EUNOMIA_VERSION}}"

mastodon_diff() {
  if [ -f "${_HERE}/client/mastodon.patch" ]; then
    cp "${_HERE}/client/mastodon.patch" "${_HERE}/client/mastodon.pre.patch"
  fi
  cd "${_HERE}/mastodon" && yarn
  # read changed files inside "${_HERE}/mastodon"

  for change in $(git status --porcelain | grep "^?" | cut -c 4-); do
    mkdir -p "${_HERE}/client/mastodon/$(dirname ${change})"
    if [ -f "${_HERE}/mastodon/${change}" ]; then
      src="${_HERE}/mastodon/${change}"
      dst="${_HERE}/client/mastodon/${change}"
      echo "${src} -> ${dst}"
      cp "${src}" "${dst}"
    elif [ -d "${_HERE}/mastodon/${change}" ]; then
      mkdir -p "${_HERE}/client/mastodon/${change}"
      src="${_HERE}/mastodon/${change}*"
      dst="${_HERE}/client/mastodon/${change}"
      echo "${src} -> ${dst}"
      cp -rp ${_HERE}/mastodon/${change}* ${_HERE}/client/mastodon/${change}
    fi
  done
  git diff --full-index --binary --ignore-blank-lines . >"${_HERE}/client/mastodon.patch"
  cd "${_HERE}" || exit
}

mastodon_patch() {
  if [ -d "${_HERE}/mastodon" ]; then
    cd "${_HERE}/mastodon"
    git checkout "${MASTODON_CHECKOUT}" && git stash
    git apply --check "${_HERE}/client/mastodon.patch"
    git apply "${_HERE}/client/mastodon.patch"
    cp -rp "${_HERE}/client/mastodon/"* "${_HERE}/mastodon/"
    cd "${_HERE}"
  fi
}

mastodon_init() {
  cd "${_HERE}" || exit
  if [ -d "${_HERE}/mastodon" ]; then
    if [ "${1}" = "--force" ]; then
      rm -rf "${_HERE}/mastodon"
    fi
  fi
  if [ ! -d "${_HERE}/mastodon" ]; then
    git clone https://github.com/mastodon/mastodon.git
  fi
  mastodon_patch
}

docker_build() {
  docker build \
    --build-arg UID="$(id -u)" \
    --build-arg GID="$(id -g)" \
    -t "${DC_IMAGE}:${DC_VERSION}" \
    mastodon
}

docker_up() {
  if [ -f "${INTEGRATED_COMPOSE_FILE}" ]; then
    docker-compose -f "${INTEGRATED_COMPOSE_FILE}" down >/dev/null 2>&1 || :
  fi
  if [ "${1}" = "--build" ] || [ "${1}" = "-b" ]; then
    docker_build
  fi
  if [ -f "${INTEGRATED_COMPOSE_FILE}" ]; then
    if docker images "${DC_IMAGE}" | grep "${DC_VERSION}"; then
      sed -i "s|\"${DC_IMAGE}.*|\"${DC_IMAGE}:${DC_VERSION}\"|g" "${INTEGRATED_COMPOSE_FILE}"
    fi
    docker-compose -f "${INTEGRATED_COMPOSE_FILE}" up -d
  fi
}

usage() {
  printf "
Usage: %s ACTION [PARAM]
\t-i|--init [--force]\t\tInit files and dirs (clone mastodon %s and apply the current patch file %s/client/mastodon.patch)
\t-d|--diff\t\t\tGenerate patch file with the diff on vanilla Mastodon
\t-p|--patch\t\t\tApply patch on vanilla mastodon cloned in ./mastodon
\t-b|--build [-u|--up]\t\tBuild and tag an image with the mastodon changes
\t-u|--up [-b|--build]\t\tStart (docker-compose up) all EUNOMIA services with the latest DC build

" "${0}" "${MASTODON_VERSION}" "${_HERE}"
}

case "${1}" in
--init | -i)
  shift
  mastodon_init "${@}"
  ;;
--diff | -d)
  mastodon_diff
  ;;
--patch | -p)
  mastodon_patch
  ;;
--build | -b)
  shift
  if [ "${1}" = "--up" ] || [ "${1}" = "-u" ]; then
    docker_up "--build"
  else
    docker_build
  fi
  ;;
--up | -u)
  shift
  docker_up "${@}"
  ;;
--help | -h)
  usage
  exit 0
  ;;
*)
  usage
  exit 1
  ;;
esac

if [ ! "${_HERE}" = "${_CWD}" ]; then
  cd "${_CWD}" || exit
fi
