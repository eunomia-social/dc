import React from 'react';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import ImmutablePureComponent from 'react-immutable-pure-component';
import ScrollableList from '../../../../components/scrollable_list';
import Column from '../../../../components/column';
import ColumnHeader from '../../../../components/column_header';
import { eunomiaIcon } from '../eunomia_icon';
import { REFACE_BASE_URL } from '../../constants';
import { makeCancelable } from '../../lib';
import Video from '../../../video';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { domainName, accessToken } from '../../../../initial_state';
import { openModal } from '../../../../actions/modal';
import { connect } from 'react-redux';
import { fromJS } from 'immutable';

const mapDispatchToProps = (dispatch) => ({
  onOpenVideo: (attachment) => {
    attachment.url = `${REFACE_BASE_URL}/reface/files/${attachment.name}?domain=${domainName}&token=${accessToken}`;
    const _attachment = fromJS(attachment);
    dispatch(openModal('VIDEO', { statusId: null, media: _attachment, index: 0, options:{} }));
  },
});
class RefaceVideos extends ImmutablePureComponent {

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    intl: PropTypes.object.isRequired,
    shouldUpdateScroll: PropTypes.func.isRequired,
    onOpenVideo: PropTypes.func.isRequired,
  };
  state = {
    media: [],
    fetched: false,
  };

  updateOnState = ['fetched', 'media'];

  constructor(props) {
    super(props);
  }

  handleOpenVideo = (attachment) => {
    this.props.onOpenVideo(attachment);
  }

  componentDidMount() {
    this.cancelablePromise = makeCancelable(
      new Promise((resolve) => {
        const url = `${REFACE_BASE_URL}/reface/videos`;
        axios.get(url, { headers: { 'X-SN-TOKEN': accessToken, 'X-SN-URL': `https://${domainName}` }, timeout: 30000 }).then(({ data }) => {
          resolve(data);
        }).catch((reason) => {
          console.warn(reason);
        });
      }).catch((reason) => {
        console.warn(reason);
      }),
    );
    this.cancelablePromise
      .promise
      .then((media) => {
        this.setState({
          media,
          fetched: true,
        });
      })
      .catch((reason) => console.warn('isCanceled', reason.isCanceled));
  }
  componentWillUnmount () {
    if (this.cancelablePromise) {
      this.cancelablePromise.cancel();
    }
  }

  setRef = (c) => {
    this.node = c;
  }

  render() {
    const { intl } = this.props;
    return (
      <Column bindToDocument label='EUNOMIA Reface'>
        <ColumnHeader
          intl={intl}
          icon='home'
          customIcon={eunomiaIcon}
          active
          title='EUNOMIA Reface'
          multiColumn={false}
          showBackButton
        />
        <ScrollableList
          trackScroll={false}
          showLoading={!this.state.fetched}
          //eslint-disable-next-line react/jsx-no-bind
          onLoadMore={() => {}}
          shouldUpdateScroll={this.props.shouldUpdateScroll}
          scrollKey='4'
          loadingMsg='Fetching Videos...'
        >
          {this.state.media.map((attachment, index) => {
            return (
              <div key={`${attachment.id}-${index}`} style={{ padding: 10 }} className='reface-mobile'>
                <Video
                  preview={`${REFACE_BASE_URL}/reface/files/${attachment.preview}?domain=${domainName}&token=${accessToken}`}
                  frameRate={attachment.frameRate}
                  blurhash={attachment.blurhash}
                  src={`${REFACE_BASE_URL}/reface/files/${attachment.name}?domain=${domainName}&token=${accessToken}`}
                  alt={attachment.title}
                  width={attachment.width}
                  height={attachment.height}
                  inline
                  //eslint-disable-next-line react/jsx-no-bind
                  onOpenVideo={()=>this.handleOpenVideo(attachment)}
                  sensitive={false}
                  alwaysVisible
                  visible
                  //eslint-disable-next-line react/jsx-no-bind
                  onToggleVisibility={()=>{}}
                  intl={intl}
                />
                <Link className='btn-link column-header__button active' to={{ pathname: '/reface/photo', attachment }}>
                  <div style={{ marginTop: 10, flexWrap: 'wrap' }}>{attachment.title}</div>
                </Link>
              </div>
            );
          })}
        </ScrollableList>
      </Column>
    );
  };

}

export default injectIntl(connect(()=> {
  return {};
}, mapDispatchToProps)(RefaceVideos));
