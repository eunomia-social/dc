# eunomia-digital-companion-mastodon

- quickstart:

```bash
git clone git@gitlab.com:eunomia-social/dc.git
cd dc
sh ./dev.sh --init
DC_VERSION="$(date +%Y%m%d)r1" sh ./dev.sh --build
# integrated with other EUNOMIA services:
INTEGRATED_COMPOSE_FILE=/path/to/integrated-eunomia/docker-compose.yml sh ./dev.sh --up 
```

- env vars:
  - `MASTODON_VERSION=3.4.1`
  - `DC_VERSION="$(date +%Y%m%d)r#"`
  - `DC_IMAGE=a.docker.registry/image:tag`
  - `INTEGRATED_COMPOSE_FILE=`
- actions:
  - `sh ./dev.sh -i|--init [--force]` Init dirs (clone mastodon ${MASTODON_VERSION} and apply the repository's patch file)
  - `sh ./dev.sh -d|--diff` Generate patch file with the diff on vanilla Mastodon
  - `sh ./dev.sh -p|--patch` Apply patch on vanilla mastodon cloned in ./mastodon
  - `sh ./dev.sh -b|--build [-u|--up]` Build and tag an image with the mastodon changes  
  - `sh ./dev.sh -u|--up [-b|--build]` Start (docker-compose up) all EUNOMIA services with the latest DC build
- [./standalone](standalone) (no ruby/db etc., only static files for desktop/electron, pwa/mobile):
  - `cd standalone && yarn start` start with live reload 
  - `cd standalone && yarn desktop` build electron(desktop) app
  - `cd standalone && yarn windows` build electron(desktop) app installer && portable executable for windows
  - `cd standalone && yarn dist` build and `rsync` the build to a remote server (for static serving/mobile webview endpoint)
  
