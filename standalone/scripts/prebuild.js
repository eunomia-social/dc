const { resolve } = require('path');
const sedPrefix = process.platform === 'darwin' ? 'sed -i ""' : 'sed -i';
const packageJson = require(resolve(__dirname, '..', 'package.json'));
const desktopPackageJson = resolve(__dirname, '..', 'desktop', 'package.json');

require('child_process').execSync(
  `${sedPrefix} "s|\\"version\\".*|\\"version\\": \\"${packageJson.version}\\",|g" "${desktopPackageJson}"`,
);

require('child_process').execSync(
  'rimraf ./public/eunomia/static/ && rimraf ./public/eunomia/manifest.json && rimraf ./public/eunomia/manifest.gz && rimraf ./public/eunomia/sw.js',
);
