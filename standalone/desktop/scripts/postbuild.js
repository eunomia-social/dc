/* eslint-disable import/no-extraneous-dependencies */
const glob = require('glob');
const { resolve } = require('path');
const fs = require('fs');
const fse = require('fs-extra');

const rootDir = resolve(__dirname, '..');
const releasesFolder = resolve(__dirname, '..', '..', 'releases');
try {
  fs.mkdirSync(releasesFolder, { recursive: true });
} catch {}
const outputs = glob.sync('build' + '**/*.{AppImage,exe,msi,deb,rpm,dmg,pacman}', { cwd: rootDir });
outputs.forEach((output) => {
  const src = resolve(__dirname, '..', output);
  const dst = resolve(releasesFolder, output.split('/')[1]);
  fs.copyFileSync(src, dst);
});
const srcDir =  resolve(__dirname, '..', 'build', 'win-unpacked');
const dstDir = resolve(releasesFolder, 'win-unpacked');
if (fs.existsSync(srcDir)) {
  fse.copySync(
    srcDir,
    dstDir,
    { overwrite: true, errorOnExist: false },
    function (err) {
      if (err) {
        console.error(err);
      }
    },
  );
}
const tsFiles = glob.sync('src' + '/*.ts', { cwd: rootDir });
tsFiles.forEach((tsFile) => {
  const jsFile = resolve(__dirname, '..', tsFile.replace('.ts', '.js').split('/')[1]);
  if (fs.existsSync(jsFile)) {
    fs.unlinkSync(jsFile);
  }
  const jsFileMap = resolve(__dirname, '..', tsFile.replace('.ts', '.js.map').split('/')[1]);
  if (fs.existsSync(jsFileMap)) {
    fs.unlinkSync(jsFileMap);
  }
});
const infoJson = resolve(__dirname, '..', 'info.json');
if (fs.existsSync(infoJson)) {
  fs.unlinkSync(infoJson);
}
require('child_process').execSync(
  'cd "' + resolve(__dirname, '..') +'" && rimraf ./build && rimraf ./public', { shell: '/bin/bash' },
);
