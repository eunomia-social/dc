// Note: You must restart bin/webpack-dev-server for changes to take effect

const path = require('path');
const { merge } = require('webpack-merge');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const OfflinePlugin = require('offline-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
// const { output } = require('./configuration');
const sharedConfig = require('./shared');

module.exports = merge(sharedConfig, {
  mode: 'production',
  devtool: 'source-map',
  stats: 'normal',
  bail: true,
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        cache: false,
        parallel: true,
        sourceMap: true,
      }),
    ],
  },

  plugins: [
    new CompressionPlugin({
      filename: '[path][name].gz[query]',
      cache: false,
      test: /\.(js|css|html|json|ico|svg|eot|otf|ttf|map)$/,
    }),
    new BundleAnalyzerPlugin({ // generates report.html
      analyzerMode: 'static',
      openAnalyzer: false,
      logLevel: 'silent',
    }),
    new WebpackPwaManifest({
      filename: 'manifest.json',
      name: 'EUNOMIA Digital Companion',
      short_name: 'EUNOMIA DC',
      description: 'EUNOMIA Digital Companion is a EUNOMIA enhanced Mastodon client',
      orientation: 'portrait',
      display: 'standalone',
      dir: 'auto',
      lang: 'en',
      start_url: '/eunomia/',
      crossorigin: 'anonymous',
      theme_color: '#000000',
      background_color: '#544795',
      categories: ['social', 'news'],
      inject: true,
      scope: '/eunomia/',
      icons: [
        {
          src: path.resolve(__dirname, '../../public/eunomia/icon.png'),
          sizes: [96, 128, 192, 256, 384, 512, 1024], // multiple sizes
          type: 'image/png',
          purpose: 'any maskable',
        },
      ],
      shortcuts: [{
        name: 'EUNOMIA Digital Companion',
        description: 'Launch EUNOMIA Digital Companion app',
        url: '/eunomia/',
        icons: [
          {
            src: '/eunomia/96.png',
            sizes: '96x96',
            type: 'image/png',
            purpose: 'any',
          },
        ],
      }],
      related_applications: [{
        'platform': 'webapp',
        'url': 'WEB_APP_URL_PLACEHOLDER/eunomia/static/manifest.json',
      }],
      prefer_related_applications: true,
      screenshots: [
        {
          src:  '/eunomia/static/screenshots/1.png',
          sizes: '1280x800',
        },
        {
          src:  '/eunomia/static/screenshots/2.png',
          sizes: '750x1334',
        },
      ],
      ios:{
        'apple-mobile-web-app-title': 'EUNOMIA Digital Companion',
        'apple-mobile-web-app-status-bar-style': 'black',
        'apple-touch-icon': '/eunomia/static/apple-touch-icon.png',
        'apple-touch-startup-image': '/eunomia/static/splash.png',
      },
    }),
    new OfflinePlugin({
      publicPath: '/eunomia/static',
      // appShell: '/eunomia/',
      safeToUseOptionalCaches: true,
      // relativePaths: true,
      // responseStrategy: 'network-first',
      // updateStrategy: 'all',
      autoUpdate: true,
      caches: {
        main: [':rest:'],
        additional: [':externals:'],
        optional: [
          '**/locale_*.js', // don't fetch every locale; the user only needs one
          '**/*_polyfills-*.js', // the user may not need polyfills
          '**/*.woff2', // the user may have system-fonts enabled
          // images/audio can be cached on-demand
          '**/*.png',
          '**/*.jpg',
          '**/*.jpeg',
          '**/*.svg',
          '**/*.mp3',
          '**/*.ogg',
        ],
      },
      externals: [
        '/eunomia/static/screenshots/1.png',
        '/eunomia/static/screenshots/2.png',
        '/eunomia/static/favicon.ico',
        '/eunomia/emoji/sheet_10.png',
        '/eunomia/sounds/boop.ogg',
        '/eunomia/sw.js',
      ],
      excludes: [
        '**/*.gz',
        '**/../',
        '**/*.map',
        'stats.json',
        'report.html',
        // any browser that supports ServiceWorker will support woff2
        '**/*.eot',
        '**/*.ttf',
        '**/*-webfont-*.svg',
        '**/*.woff',
      ],
      ServiceWorker: {
        entry: path.join(__dirname, '../../app/src/service_worker/entry.js'),
        cacheName: 'eunomia',
        output: '../sw.js',
        scope: '/eunomia/',
        publicPath: '/eunomia/sw.js',
        minify: true,
      },
    }),
  ],
});
