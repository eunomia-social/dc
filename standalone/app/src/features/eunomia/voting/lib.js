import { NEW_VOTING_LIB } from '../constants';
import voting_new from './voting_new/lib';
import voting_old from './voting_old/lib';

export default NEW_VOTING_LIB ? voting_new: voting_old;
