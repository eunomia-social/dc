const headDarkStyleRegExp = /(<!--)(<link href="\/eunomia\/static\/css\/eunomia-light-)(.*)(rel="stylesheet">)(-->)/;
const getThemeFromHeader = () => {
  const headContent = document.head.innerHTML;
  if (headContent.includes("mastodon-light-") || headContent.includes("eunomia-light-")) {
    return 'light';
  }
  if (headDarkStyleRegExp.test(headContent)) {
    return 'dark';
  }
  return 'light';
};

function onLoad() {
  try {
    const themeName = getThemeFromHeader();
    let els = document.querySelectorAll('a[href="/eunomia/terms"]');
    for (let i = 0, l = els.length; i < l; i++) {
      const el = els[i];
      el.setAttribute('href', `/eunomia/terms?theme=${themeName}`);
    }
  } catch {}
}
document.addEventListener("DOMContentLoaded", onLoad);
