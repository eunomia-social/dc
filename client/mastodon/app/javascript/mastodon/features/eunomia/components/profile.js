import React from 'react';
import { connect } from 'react-redux';
import { expandHomeTimeline } from '../../../actions/timelines';
import PropTypes from 'prop-types';
import Column from '../../../components/column';
import ColumnHeader from '../../../components/column_header';
import { addColumn, removeColumn, moveColumn } from '../../../actions/columns';
import ColumnSettingsContainer from '../containers/column_settings';
import { defineMessages, injectIntl } from 'react-intl';
import { fetchAnnouncements, toggleShowAnnouncements } from '../../../actions/announcements';
import AnnouncementsContainer from '../../../features/getting_started/containers/announcements_container';
import classNames from 'classnames';
import IconWithBadge from '../../../components/icon_with_badge';
import Textarea from 'react-textarea-autosize';
import Button from '../../../components/button';
import { myUrl } from '../../../initial_state';

const messages = defineMessages({
  title: { id: 'column.profile', defaultMessage: 'Profile' },
  show_announcements: { id: 'home.show_announcements', defaultMessage: 'Show announcements' },
  hide_announcements: { id: 'home.hide_announcements', defaultMessage: 'Hide announcements' },
});

const mapStateToProps = state => ({
  hasUnread: state.getIn(['timelines', 'home', 'unread']) > 0,
  isPartial: state.getIn(['timelines', 'home', 'isPartial']),
  hasAnnouncements: !state.getIn(['announcements', 'items']).isEmpty(),
  unreadAnnouncements: state.getIn(['announcements', 'items']).count(item => !item.get('read')),
  showAnnouncements: state.getIn(['announcements', 'show']),
});

export default @connect(mapStateToProps)
@injectIntl
class Profile extends React.PureComponent {

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    shouldUpdateScroll: PropTypes.func,
    intl: PropTypes.object.isRequired,
    hasUnread: PropTypes.bool,
    isPartial: PropTypes.bool,
    columnId: PropTypes.string,
    multiColumn: PropTypes.bool,
    hasAnnouncements: PropTypes.bool,
    unreadAnnouncements: PropTypes.number,
    showAnnouncements: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.onDisplayNameChange = this.onDisplayNameChange.bind(this);
    this.onBioChange = this.onBioChange.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }

  state = {
    displayName: '',
    bio: '',
  };

  handlePin = () => {
    const { columnId, dispatch } = this.props;

    if (columnId) {
      dispatch(removeColumn(columnId));
    } else {
      dispatch(addColumn('EUNOMIA', {}));
    }
  }

  handleMove = (dir) => {
    const { columnId, dispatch } = this.props;
    dispatch(moveColumn(columnId, dir));
  }

  handleHeaderClick = () => {
    this.column.scrollTop();
  }

  setRef = c => {
    this.column = c;
  }

  handleLoadMore = maxId => {
    this.props.dispatch(expandHomeTimeline({ maxId }));
  }

  componentDidMount () {
    this.props.dispatch(fetchAnnouncements());
    this._checkIfReloadNeeded(false, this.props.isPartial);
  }

  componentDidUpdate (prevProps) {
    this._checkIfReloadNeeded(prevProps.isPartial, this.props.isPartial);
  }

  componentWillUnmount () {
    this._stopPolling();
  }

  _checkIfReloadNeeded (wasPartial, isPartial) {
    const { dispatch } = this.props;

    if (wasPartial === isPartial) {
      return;
    }
    if (!wasPartial && isPartial) {
      this.polling = setInterval(() => {
        dispatch(expandHomeTimeline());
      }, 3000);
    } else if (wasPartial && !isPartial) {
      this._stopPolling();
    }
  }

  _stopPolling () {
    if (this.polling) {
      clearInterval(this.polling);
      this.polling = null;
    }
  }

  handleToggleAnnouncementsClick = (e) => {
    e.stopPropagation();
    this.props.dispatch(toggleShowAnnouncements());
  }

  handleSave = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };

  onDisplayNameChange = (e) => {
    this.setState({ displayName: e.target.value });
  };

  onBioChange = (e) => {
    this.setState({ bio: e.target.value });
  };

  render () {
    const { intl, hasUnread, columnId, multiColumn, hasAnnouncements, unreadAnnouncements, showAnnouncements } = this.props;
    const pinned = !!columnId;

    let announcementsButton = null;

    if (hasAnnouncements) {
      announcementsButton = (
        <button
          className={classNames('column-header__button', { 'active': showAnnouncements })}
          title={intl.formatMessage(showAnnouncements ? messages.hide_announcements : messages.show_announcements)}
          aria-label={intl.formatMessage(showAnnouncements ? messages.hide_announcements : messages.show_announcements)}
          aria-pressed={showAnnouncements ? 'true' : 'false'}
          onClick={this.handleToggleAnnouncementsClick}
        >
          <IconWithBadge id='bullhorn' count={unreadAnnouncements} />
        </button>
      );
    }

    return (
      <Column bindToDocument={!multiColumn} ref={this.setRef} label={intl.formatMessage(messages.title)}>
        <ColumnHeader
          intl={intl}
          icon='user'
          active={hasUnread}
          title={intl.formatMessage(messages.title)}
          onPin={this.handlePin}
          onMove={this.handleMove}
          onClick={this.handleHeaderClick}
          pinned={pinned}
          multiColumn={multiColumn}
          extraButton={announcementsButton}
          appendContent={hasAnnouncements && showAnnouncements && <AnnouncementsContainer />}
        >
          <ColumnSettingsContainer />
        </ColumnHeader>
        <div className='scrollable settings-wrapper simple_form' style={{ background: 'transparent' }}>
          <div className='content-wrapper'>
            <div className='content' style={{ padding: '20px 0px' }}>
              <div className='fields-row__column fields-group fields-row__column-12'>
                <div className='input with_label string optional'>
                  <div className='label_input'>
                    <label className='string optional' htmlFor={'display-name'}>Display name</label>
                    <div className='label_input__wrapper'>
                      <input
                        type='text'
                        placeholder=''
                        id={'display-name'}
                        value={this.state.displayName}
                        onChange={this.onDisplayNameChange}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className='fields-row__column fields-group fields-row__column-12'>
                <div className='input with_label optional'>
                  <div className='label_input'>
                    <label className='optional' htmlFor={'account-bio'}>Bio</label>
                    <Textarea
                      id={'account-bio'}
                      placeholder=''
                      value={this.state.bio}
                      onChange={this.onBioChange}
                      minRows={3}
                    />
                  </div>
                </div>
              </div>
              <div className='fields-row__column fields-group fields-row__column-12'>
                <div className='input with_label optional'>
                  <div className='label_input'>
                    <label className='optional' htmlFor={'account-bio'}>Header</label>
                  </div>
                </div>
              </div>
              <div className='fields-row__column fields-group fields-row__column-12'>
                <div className='input with_label optional'>
                  <div className='label_input'>
                    <label className='optional' htmlFor={'account-bio'}>Avatar</label>
                  </div>
                </div>
              </div>
              <div className='fields-row__column fields-group fields-row__column-12'>
                <div className='input with_label boolean optional account_bot field_with_hint'>
                  <div className='label_input'>
                    <label className='boolean optional' htmlFor='account_locked'>Lock account</label>
                    <div className='label_input__wrapper'>
                      <label className='checkbox'>
                        <input className='boolean optional' type='checkbox' value='1' name='account[locked]' id='account_locked' />
                      </label>
                    </div>
                  </div>
                  <span className='hint'>Requires you to manually approve followers</span>
                </div>
              </div>
              <div className='fields-row__column fields-group fields-row__column-12'>
                <div className='input with_label boolean optional account_bot field_with_hint'>
                  <div className='label_input'>
                    <label className='boolean optional' htmlFor='account_bot'>This is a bot account</label>
                    <div className='label_input__wrapper'>
                      <label className='checkbox'>
                        <input className='boolean optional' type='checkbox' value='1' name='account[bot]' id='account_bot' />
                      </label>
                    </div>
                  </div>
                  <span className='hint'>This account mainly performs automated actions and might not be monitored</span>
                </div>
              </div>
              <div className='fields-row__column fields-group fields-row__column-12'>
                <div className='input with_label optional'>
                  <div className='label_input'>
                    <label className='optional' htmlFor={'account-meta'}>Profile metadata</label>
                    <p className='hint'>You can have up to 4 items displayed as a table on your profile</p>
                  </div>
                </div>
              </div>
              <div className='fields-row__column fields-group fields-row__column-12'>
                <div className='row'>
                  <div className='input string optional account_fields_name'><input
                    maxLength='255'
                    className='string optional'
                    placeholder='Label' size='255'
                    type='text' value=''
                    name='account[fields_attributes][0][name]'
                    id='account_fields_attributes_0_name'
                  />
                  </div>
                  <div className='input string optional account_fields_value'><input
                    maxLength='255'
                    className='string optional'
                    placeholder='Content' size='255'
                    type='text' value=''
                    name='account[fields_attributes][0][value]'
                    id='account_fields_attributes_0_value'
                  />
                  </div>
                </div>
                <div className='row'>
                  <div className='input string optional account_fields_name'>
                    <input
                      maxLength='255'
                      className='string optional'
                      placeholder='Label' size='255'
                      type='text' value=''
                      name='account[fields_attributes][0][name]'
                      id='account_fields_attributes_0_name'
                    />
                  </div>
                  <div className='input string optional account_fields_value'><input
                    maxLength='255'
                    className='string optional'
                    placeholder='Content' size='255'
                    type='text' value=''
                    name='account[fields_attributes][1][value]'
                    id='account_fields_attributes_1_value'
                  />
                  </div>
                </div>
                <div className='row'>
                  <div className='input string optional account_fields_name'><input
                    maxLength='255'
                    className='string optional'
                    placeholder='Label' size='255'
                    type='text' value=''
                    name='account[fields_attributes][2][name]'
                    id='account_fields_attributes_2_name'
                  />
                  </div>
                  <div className='input string optional account_fields_value'><input
                    maxLength='255'
                    className='string optional'
                    placeholder='Content' size='255'
                    type='text' value=''
                    name='account[fields_attributes][2][value]'
                    id='account_fields_attributes_2_value'
                  />
                  </div>
                </div>
                <div className='row'>
                  <div className='input string optional account_fields_name'><input
                    maxLength='255'
                    className='string optional'
                    placeholder='Label' size='255'
                    type='text' value=''
                    name='account[fields_attributes][3][name]'
                    id='account_fields_attributes_3_name'
                  />
                  </div>
                  <div className='input string optional account_fields_value'><input
                    maxLength='255'
                    className='string optional'
                    placeholder='Content' size='255'
                    type='text' value=''
                    name='account[fields_attributes][3][value]'
                    id='account_fields_attributes_3_value'
                  />
                  </div>
                </div>
              </div>
              <div className='fields-row__column fields-group fields-row__column-12'>
                <div className='input with_label optional'>
                  <div className='label_input'>
                    <label className='optional' htmlFor={'account-bio'}>Verification</label>
                    <p className='hint'>You can <strong>verify yourself as the owner of the links in your profile
                      metadata</strong>. For that, the linked website must contain a link back to your Mastodon profile.
                      The link back <strong>must</strong> have a <code>rel="me"</code> attribute. The text content of
                      the link does not matter. Here is an example:</p>
                    <div className='input-copy'>
                      <div className='input-copy__wrapper'>
                        <input
                          maxLength='999' readOnly='true' spellCheck='false' type='text'
                          value={`<a rel="me" href="${myUrl}">Mastodon</a>`}
                        />
                      </div>
                      <button type='button'>Copy</button>
                    </div>
                  </div>
                </div>
              </div>
              <div className='fields-row__column fields-group fields-row__column-12'>
                <Button text={'SAVE CHANGES'} onClick={this.handleSave} />
              </div>
            </div>
          </div>
        </div>
      </Column>
    );
  }

}
