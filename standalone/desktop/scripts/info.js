const { writeFileSync } = require('fs');
const { resolve } = require('path');

const projectPackageJson = require(resolve(__dirname, '..', '..', 'package.json'));
const desktopPackageJson = require(resolve(__dirname, '..', './package.json'));

const versions = {
  name: desktopPackageJson.build.productName,
  version: desktopPackageJson.version,
  description: projectPackageJson.description,
  copyright: projectPackageJson.copyright,
};

writeFileSync(
  resolve(__dirname, '..', 'build', 'info.json'),
  JSON.stringify(versions),
);
