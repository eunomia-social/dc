import {
  SET_BROWSER_SUPPORT,
  SET_SUBSCRIPTION,
  CLEAR_SUBSCRIPTION,
  SET_ALERTS,
  setAlerts,
} from './setter';
import { register, saveSettings, checkOSXNotifications } from './registerer';

export {
  SET_BROWSER_SUPPORT,
  SET_SUBSCRIPTION,
  CLEAR_SUBSCRIPTION,
  SET_ALERTS,
  register,
  checkOSXNotifications,
};

export function changeAlerts(path, value) {
  return dispatch => {
    dispatch(setAlerts(path, value));
    dispatch(saveSettings());
  };
}
