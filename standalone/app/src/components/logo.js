import React from 'react';
import eunomiaSvg from '../../images/icon_eunomia.svg';

const Logo = () => (
  <img className='fa-fw svg-icon' style={{ width: 60 }} src={eunomiaSvg} alt='EUNOMIA' />
);

export default Logo;
