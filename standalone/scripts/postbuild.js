/* eslint-disable import/no-extraneous-dependencies */
const fse = require('fs-extra');
const { resolve, join } = require('path');

const sedPrefix = process.platform === 'darwin' ? 'sed -i ""' : 'sed -i';
const rootDir = resolve(__dirname, '..', 'public', 'eunomia');
const staticDir = join(rootDir, 'static');
const screenshotsDir = join(__dirname, '..', 'screenshots');

fse.copyFileSync(join(rootDir, 'apple-touch-icon.png'), join(staticDir, 'apple-touch-icon.png'));
fse.copyFileSync(join(rootDir, 'splash.png'), join(staticDir, 'splash.png'));
fse.copyFileSync(join(rootDir, 'ocr', 'eng.traineddata.gz'), join(staticDir, 'ocr', 'eng.traineddata.gz'));
const srcDir =  join(rootDir, 'extra-static');
if (fse.existsSync(srcDir)) {
  fse.copySync(
    srcDir,
    staticDir,
    { overwrite: true, errorOnExist: false },
    function (err) {
      if (err) {
        console.error(err);
        process.exit(1);
      }
    },
  );
}
fse.copySync(
  screenshotsDir,
  join(staticDir, 'screenshots' ),
  { overwrite: true, errorOnExist: false },
  function (err) {
    if (err) {
      console.error(err);
      process.exit(1);
    }
  },
);

let webAppUrl = '';

const dotEnv = resolve(__dirname, '..', '.env');
if (fse.existsSync(dotEnv)) {
  require('dotenv').config({
    path: dotEnv,
  });
  webAppUrl = process.env.REMOTE_DOMAIN;
}
require('child_process').execSync(
  `${sedPrefix} 's|WEB_APP_URL_PLACEHOLDER|https://${webAppUrl}|g' "${join(staticDir, 'manifest.json')}"`,
);
// require('child_process').execSync(
//   `${sedPrefix} "s|rel=\\"stylesheet\\">|rel=\\"preload\\" as=\\"style\\" onload=\\"this.onload=null;this.rel='stylesheet'\\">|g" ${indexHtml}`,
// );
// '/"\/eunomia\/static\/\.\.\/"/d'
