const termsDarkStyleRegExp = /(<!--)(<link href="\/eunomia\/static\/css\/eunomia-light-)(.*)(rel="stylesheet">)(-->)/;
const termsGetTheme = () => {
  const headContent = document.head.innerHTML;
  if (termsDarkStyleRegExp.test(headContent)) {
    return 'dark';
  }
  return 'light';
};

if (termsGetTheme() === 'light') {
    window.localStorage.setItem('EUNOMIA_MASTODON_THEME', 'light');
} else {
    window.localStorage.setItem('EUNOMIA_MASTODON_THEME', 'dark');
}
