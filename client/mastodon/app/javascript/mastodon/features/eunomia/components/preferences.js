import React from 'react';
import PropTypes from 'prop-types';
import Column from '../../../components/column';
import ColumnHeader from '../../../components/column_header';
import { defineMessages, injectIntl } from 'react-intl';
import { eunomiaIcon } from './eunomia_icon';
import Button from '../../../components/button';
import { getTheme, setTheme } from '../theme';
import { domainName } from '../../../initial_state';
// import { defineMessages, FormattedMessage, injectIntl } from 'react-intl';
// import Toggle from 'react-toggle';
const messages = defineMessages({
  title: { id: 'column.eunomia', defaultMessage: 'EUNOMIA Preferences' },
});

export default @injectIntl
class EunomiaSettings extends React.PureComponent {

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    intl: PropTypes.object.isRequired,
  };

  setRef = c => {
    this.column = c;
  }

  constructor(props) {
    super(props);
    this.setTheme = this.setTheme.bind(this);
    this.toggleEnableChange = this.toggleEnableChange.bind(this);
    this.togglePrefChange = this.togglePrefChange.bind(this);
    this.handleDownloadAction = this.handleDownloadAction.bind(this);
    this.handleDeleteAction = this.handleDeleteAction.bind(this);
  }

  setTheme = (e) => {
    this.setState({ theme: e.target.value });
  };

  state = {
    theme: getTheme(),
  };

  handleDownloadAction = (e) => {
    e.preventDefault();
    e.stopPropagation();
    // this.context.router.history.push('/profile');
  }

  handleDeleteAction = (e) => {
    e.preventDefault();
    e.stopPropagation();
    // this.context.router.history.push('/profile');
  };

  toggleEnableChange = () => {
    this.setState({ scoringRulesEnabled: !this.state.scoringRulesEnabled });
  }

  togglePrefChange = () => {
    //
  }

  handleSaveTheme = (e) => {
    e.stopPropagation();
    setTheme(this.state.theme, true, () => {
      window.location.reload();
    });
  };

  render () {
    const { intl } = this.props;

    return (
      <Column bindToDocument ref={this.setRef} label={intl.formatMessage(messages.title)}>
        <ColumnHeader
          intl={intl}
          icon='home'
          showBackButton
          customIcon={eunomiaIcon}
          active={false}
          title={intl.formatMessage(messages.title)}
          multiColumn={false}
        />
        <div className='scrollable settings-wrapper simple_form'>
          <div className='content-wrapper'>
            <div className='content' style={{ padding: '20px 10px' }}>
              <div className='fields-row'>
                <div className='fields-group fields-row__column fields-row__column-12'>
                  <div className='input with_label select optional user_setting_theme'>
                    <div className='label_input'>
                      <label
                        className='select optional'
                        htmlFor='user_setting_theme'
                      >
                        <h4>Theme</h4>
                      </label>
                      <div className='label_input__wrapper'>
                        <select
                          className='select optional'
                          id='user_setting_theme'
                          value={this.state.theme}
                          onChange={this.setTheme}
                        >
                          <option value='dark'>Dark</option>
                          <option value='light'>Light</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <Button text={'SAVE THEME'} onClick={this.handleSaveTheme} />
              <h4>Mastodon Settings</h4>
              <p className='muted-hint'>
                You can view and modify your Mastodon settings <a href={`https://${domainName}/settings`} target='_blank' rel='noopener noreferrer nofollow'>here</a>
              </p>
            </div>
          </div>
        </div>
      </Column>
    );
  }

}

