/* eslint-disable no-console */
const { resolve } = require('path');
const home = process.env.HOME;
const rootDir = resolve(__dirname, '..');
const userId = require('child_process').execSync('id -u', { shell: '/bin/bash', encoding: 'UTF-8' }).replace('\n', '');
const groupId = require('child_process').execSync('id -g', { shell: '/bin/bash', encoding: 'UTF-8' }).replace('\n', '');

const owner = `${userId}:${groupId}`;
const command = 'cd ' + rootDir + ' && docker run --rm \
 --env-file <\(env | grep -iE "DEBUG|NODE_|ELECTRON_|YARN_|NPM_|BUILD_"\)  \
 --env ELECTRON_CACHE="/root/.cache/electron" \
 --env ELECTRON_BUILDER_CACHE="/root/.cache/electron-builder" \
 -v \${PWD##*/}-node-modules:/root/node_modules \
 -v \${PWD}:/project \
 -v '+ home + '/.cache/electron:/root/.cache/electron \
 -v '+ home + '/.cache/electron-builder:/root/.cache/electron-builder \
 electronuserland/builder:wine bash -c \'chown -R ' + owner + ' /project && yarn --cwd desktop windows && chown -R ' + owner + ' /project\'';
const output=require('child_process').execSync(command, { shell: '/bin/bash', encoding: 'UTF-8' });

console.log(output);
