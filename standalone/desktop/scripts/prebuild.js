/* eslint-disable import/no-extraneous-dependencies */
const fse = require('fs-extra');
const { resolve } = require('path');

const rootDir = resolve(__dirname, '..');
try {
  require('child_process').execSync(
    'cd "' + rootDir +'" && mkdir -p build && mkdir -p public && ./node_modules/.bin/rimraf ./build && ./node_modules/.bin/rimraf ./public && yarn compile && node scripts/info.js && cp build/* . && yarn --cwd .. build',
    { encoding: 'utf8' });
} catch (reason) {
  console.error(reason);
  process.exit(1);
}

fse.copySync(resolve(__dirname, '..', '..', 'public'), resolve(__dirname, '..', 'public'), { errorOnExist: false, overwrite: true });
fse.copyFileSync(resolve(__dirname, '..', '..', 'public', 'eunomia', 'icon.ico'), resolve(__dirname, '..', 'build', 'icon.ico'));
