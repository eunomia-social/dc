import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { connect } from 'react-redux';
import { fromJS } from 'immutable';

import { openModal } from '../../../../actions/modal';
import ImmutablePureComponent from 'react-immutable-pure-component';
import ScrollableList from '../../../../components/scrollable_list';
import Column from '../../../../components/column';
import ColumnHeader from '../../../../components/column_header';
import { eunomiaIcon } from '../eunomia_icon';
import { REFACE_BASE_URL } from '../../constants';
import Video from '../../../video';
import IconButton from '../../../../components/icon_button';
import { domainName, accessToken } from '../../../../initial_state';
import { injectIntl } from 'react-intl';

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch) => ({
  onOpenVideo: (result) => {
    result.url = `${REFACE_BASE_URL}/reface/files/${result.name}?domain=${domainName}&token=${accessToken}`;
    const attachment = fromJS(result);
    dispatch(openModal('VIDEO', { statusId: null, media: attachment, index: 0, options:{} }));
  },
});

class RefaceResults extends ImmutablePureComponent {

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    intl: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    onOpenVideo: PropTypes.func.isRequired,
    shouldUpdateScroll: PropTypes.func.isRequired,
  };

  state = {
    media: [],
    fetched: false,
  };

  _mounted = false;
  _interval = null;
  updateOnState = ['fetched', 'media'];

  constructor(props) {
    super(props);
    this._checkStatus = this._checkStatus.bind(this);
    this.deleteResult = this.deleteResult.bind(this);
    this.shareResult = this.shareResult.bind(this);
    this.handleOpenVideo = this.handleOpenVideo.bind(this);
  }

  handleOpenVideo = (result) => {
    this.props.onOpenVideo(result);
  }

  _checkStatus() {
    if (this._mounted && !this.state.fetched) {
      const { task } = this.context.router.route.location.state;
      const url =  `${REFACE_BASE_URL}/reface/status/${task}`;
      axios.get(url, {
        headers: {
          'X-SN-TOKEN': accessToken,
          'X-SN-URL': `https://${domainName}`,
        },
      }).then(({ data }) => {
        if (this._mounted) {
          const { status } = data;
          if (!status || status === 'not_found') {
            this.context.router.history.goBack();
          } else {
            if (status === 'complete') {
              if (!data.result) {
                this.context.router.history.goBack();
              } else {
                this.setState({
                  media: [data.result],
                  fetched: true,
                });
              }
            }
          }
        }
      }).catch((reason) => {
        console.warn(reason);
        this.context.router.history.goBack();
      });
    }
  }

  componentDidMount() {
    const { task } = this.context.router.route.location.state;
    if (!task) {
      this.context.router.history.goBack();
    } else {
      this._interval = setInterval(this._checkStatus, 3000);
      this._mounted = true;
    }
  }

  componentWillUnmount () {
    if (this._interval) {
      clearInterval(this._interval);
    }
    this._mounted = false;
  }

  setRef = (c) => {
    this.node = c;
  }

  shareResult() {
    if (this.state.media && this.state.media.length === 1) {
      const filename = this.state.media[0].name;
      const url = `${REFACE_BASE_URL}/reface/share/${filename}`;
      axios.post(url, null, {
        headers: { 'X-SN-TOKEN': accessToken, 'X-SN-URL': `https://${domainName}` }, timeout: 30000,
      }).then(({ data }) => {
        if (data.id){
          this.context.router.history.push(`/statuses/${data.id}`);
        } else {
          this.context.router.history.push('/timelines/home');
        }
      }).catch((reason)=> {
        console.warn(reason);
        this.context.router.history.goBack();
      });
    } else {
      this.context.router.history.goBack();
    }
  }

  deleteResult() {
    if (this.state.media && this.state.media.length === 1) {
      const filename = this.state.media[0].name;
      const url = `${REFACE_BASE_URL}/reface/files/${filename}`;
      axios.delete(url, {
        headers: { 'X-SN-TOKEN': accessToken, 'X-SN-URL': `https://${domainName}` }, timeout: 30000,
      }).then(() => {
        this.context.router.history.goBack();
      }).catch((reason)=> {
        console.warn(reason);
        this.context.router.history.goBack();
      });
    } else {
      this.context.router.history.goBack();
    }
  }

  render() {
    const { intl } = this.props;
    return (
      <Column bindToDocument label='EUNOMIA Reface'>
        <ColumnHeader
          intl={intl}
          icon='home'
          customIcon={eunomiaIcon}
          active
          title='EUNOMIA Reface'
          multiColumn={false}
          showBackButton
        />
        <ScrollableList
          trackScroll={false}
          showLoading={!this.state.fetched}
          //eslint-disable-next-line react/jsx-no-bind
          onLoadMore={() => {}}
          shouldUpdateScroll={this.props.shouldUpdateScroll}
          loadingMsg={'Swapping....'}
          scrollKey='4'
        >
          {this.state.media.map((result, index) => {
            return (
              <div key={`${result.id}-${index}`} style={{ padding: 10 }} className='reface-mobile'>
                <Video
                  preview={`${REFACE_BASE_URL}/reface/files/${result.preview}?domain=${domainName}&token=${accessToken}`}
                  frameRate={result.frameRate}
                  blurhash={result.blurhash}
                  src={`${REFACE_BASE_URL}/reface/files/${result.name}?domain=${domainName}&token=${accessToken}`}
                  alt={result.title}
                  width={result.width}
                  height={result.height}
                  inline
                  //eslint-disable-next-line react/jsx-no-bind
                  onOpenVideo={() => this.handleOpenVideo(result)}
                  sensitive={false}
                  alwaysVisible
                  visible
                  //eslint-disable-next-line react/jsx-no-bind
                  onToggleVisibility={()=>{}}
                  intl={intl}
                />
                <div className='reface_controls status__action-bar' style={{ marginTop: 10 }} >
                  <IconButton
                    className='status__action-bar-button'
                    size={24}
                    title={'Delete'}
                    icon={'remove'}
                    onClick={this.deleteResult}
                  />
                  <IconButton
                    className='status__action-bar-button'
                    size={24}
                    title={'Share'}
                    icon={'share-alt'}
                    onClick={this.shareResult}
                  />
                </div>

              </div>
            );
          })}
        </ScrollableList>
      </Column>
    );
  };

}

export default injectIntl(connect(() => {
  return {};
}, mapDispatchToProps)(RefaceResults));
