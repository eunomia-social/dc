## Web / PWA
- `yarn start` (dev)
- `yarn build` (you can then serve the [./public](./public) folder)
- `yarn rsync` (rsync the public folder to a remote, see .gitingored, env.sample). <br>
to rsync `./public` to `/path/to/remote/public` <br>use: `RSYNC_PATH=/path/to/remote` (without the trailing `/public`)
- `yarn dist`: `yarn build && yarn rsync`
## Desktop (electron)
- `yarn desktop` (outputs: ./releases/DC-{version}.{dmg,AppImage,deb,rpm})
- (tested on linux with docker): `yarn windows` (outputs: ./releases/DC-{version}{.Portable.exe,Installer.exe})
- for Windows store: (on linux with docker) `yarn windows`: output: ./releases/win-unpacked. use electron-windows-store to generate .appx

## .gitignored files:
- To 'rsync' the public folder (after building) and set the start_url,url for PWA:
    -  ./.env [./env.sample](./env.sample)
- For electron-notarize (mac):
    -  ./desktop/.env [./desktop/env.sample](./desktop/env.sample)
    -  ./desktop/dc.provisionprofile
- For the reface feature, you need to provide the REACT_APP_REFACE_BASE_URL
  - ./.env [./env.sample](./env.sample)
