import React from 'react';
import euLogo from '../../../../images/europe.jpg';

export function terms () {
  return  (
    <div className={'rich-formatting terms-modal-content'}>
      <h2>DATA PROTECTION NOTICE AND CONSENT FOR PARTICIPATION IN RESEARCH</h2>
      <div style={{ marginBottom: 10 }}>
        <h3>Information on the project</h3>
      </div>
      <div style={{ marginBottom: 10 }}>
        <p style={{ marginBottom: 10 }}>
          The <a href='https://eunomia.social' target='_blank' rel='nofollow noopener noreferrer'>EUNOMIA project</a>
          {' '}aims to assist social media users in determining the trustworthiness
          of information on social media platforms.To this end, it provides a
          toolkit integrated in decentralised social media platforms offering
          trustworthiness indicators and enabling users to and express their
          trust on social media posts. Participating in this research you will
          have the opportunity to experience the EUNOMIA toolkit and provide
          your feedback to further develop its features. You will need to sign
          up on EUNOMIA instance of Mastodon federated platform and act as you
          would normally do in your personal social media (i.e, post, comment,
          share) using also EUNOMIA’s features such as trust/don’t trust
          button and other features. Note that there may or may not be, at
          random times, false information injected into the EUNOMIA instance
          by different users (that may or may not be controlled by the
          researchers), in order to evaluate user behaviour and responses.
        </p>
        <p>
          EUNOMIA unites a highly complementary consortium of 10 partners from 9
          European countries from cross-disciplines and cross-sectors including
          academic, decentralised social media, public journalism organisations
          and SMEs. Specifically, University of Greenwich, Blasting News,
          Trilateral Research, SYNYO, INOV, University of West Attica,
          University of Nicosia, ORF, Eugen Rochko (creator of Mastodon),
          SIMAVI. The joint data controllership is between EUNOMIA project
          partners: University of Greenwich, INOV, University of West Attica and
          University of Nicosia.
        </p>
      </div>
      <h3>What information do we collect?</h3>
      <div>
        <p style={{ marginBottom: 10 }}>
          EUNOMIA instance collects all information collected from Mastodon platform:
        </p>
        <p style={{ marginBottom: 10 }}>
          <span style={{ textDecoration: 'underline' }}>Basic account information:</span> When you register on a
          Mastodon node, you are asked to enter a username, an e-mail address and a password. You may also enter
          additional profile information such as a display name and biography, and upload a profile picture and header
          image. The username, display name, biography, profile picture and header image are always listed publicly.
        </p>
        <p style={{ marginBottom: 10 }}>
          <span style={{ textDecoration: 'underline' }}>Posts, following and other public information:</span> The list
          of people you follow is listed publicly, the same is true for your followers. When you submit a message,
          the date and time is stored as well as the application you submitted the message from. Messages may contain
          media attachments, such as pictures and videos. Public and unlisted posts are available publicly.
          When you feature a post on your profile, that is also publicly available information. Your posts are
          delivered to your followers, in some cases it means they are delivered to different servers and copies are s
          tored there. When you delete posts, this is likewise delivered to your followers.
          The action of reblogging or favouriting another post is always public.
        </p>
        <p style={{ marginBottom: 10 }}>
          <span style={{ textDecoration: 'underline' }}>Direct and followers-only posts:</span> All posts are stored and processed on the server. Followers-only posts are delivered to your followers and users who are mentioned in them, and direct posts are delivered only to users mentioned in them. In some cases, it means they are delivered to different servers and copies are stored there. We make a good faith effort to limit the access to those posts only to authorized persons, but other servers may fail to do so. Therefore, it's important to review servers your followers belong to. You may toggle an option to approve and reject new followers manually in the settings. Please keep in mind that the operators of the server and any receiving server may view such messages, and that recipients may screenshot, copy or otherwise re-share them. Do not share any dangerous information over Mastodon.
        </p>
        <p style={{ marginBottom: 10 }}>
          <span style={{ textDecoration: 'underline' }}>IPs and other metadata:</span> When you log in, we record the IP address you log in from, as well as the name of your browser application. All the logged in sessions are available for your review and revocation in the settings. The latest IP address used is stored for up to 12 months. We also may retain server logs which include the IP address of every request to our server.
        </p>
        <p style={{ marginBottom: 10 }}>
          Additionally, participants’ trustworthiness votes will be collected to be displayed and analysed to inform further indicators. The trustworthiness scoring results solely on participants’ actions with no interference from the researchers. For the purposes of the study, EUNOMIA researchers will also collect participants’ feedback on the toolkit.
        </p>
        <p style={{ marginBottom: 10 }}>
          We recognize the importance to protect and secure the participants’ personal data and we are committed to adopt and implement all appropriate technical and organizational measures for the security and protection of the participants’ personal data. It is necessary to process such personal data for carrying out the research in a proper and effective manner. However, participation in the research project is done so on a voluntary basis and based on the participant’s consent.
        </p>
      </div>
      <div style={{ marginBottom: 10, marginTop: 20 }}>
        <h3>What do we use your information for?</h3>
      </div>
      <div>
        <p style={{ marginBottom: 10 }}>
          Any of the information we collect from you may be used to provide the core functionalities of EUNOMIA toolkit and its further development. We rely on your consent as the lawful basis for doing this.
        </p>
        <p style={{ marginBottom: 10 }}>
          During various phases of the experiment, participants will be contacted by the researchers to collect feedback on the EUNOMIA toolkit, which will be used to improve the ongoing experience and to inform results and evaluation. Note that the email address you provide is necessary so that we may facilitate the setting up of your account to participate in the research. We also use email to provide you with project related information and feedback.
        </p>
        <p style={{ marginBottom: 10 }}>
          Note, the administrator of the Mastodon instance reserves the right to remove a post/ toot and ultimately delete an account, after giving suitable warnings, when there are abusive or inappropriate posts/ toots.
        </p>
      </div>
      <div style={{ marginBottom: 10, marginTop: 20 }}>
        <h3>How do we protect your information?</h3>
      </div>
      <div>
        <p style={{ marginBottom: 10 }}>
          We will pseudonymise and where possible anonymise the data you provide us to the extent possible when and if they will be used, removing all personally identifiable information where appropriate. We will only collect and process data that is strictly necessary for running the research, for our internal processing, administrative purposes, and to enable us to contact you if we require further information.
        </p>
      </div>
      <div style={{ marginBottom: 10, marginTop: 20 }}>
        <h3>What is our data retention policy?</h3>
      </div>
      <div>
        <p style={{ marginBottom: 10 }}>
          The retention policy follows the one of Mastodon making sure your posts and online interaction will not be disrupted at the end of the project. See following from Mastodon’s privacy policy:
        </p>
        <p style={{ marginBottom: 10 }}>
          We will make a good faith effort to:
        </p>
        <div style={{ marginBottom: 10 }}>
          <ul>
            <li>
              Retain server logs containing the IP address of all requests to this server, in so far as such logs are kept, no more than 90 days.
            </li>
            <li>
              Retain the IP addresses associated with registered users no more than 12 months.
            </li>
          </ul>
        </div>
        <p style={{ marginBottom: 10 }}>
          You can request and download an archive of your content, including your posts, media attachments, profile picture, and header image.
        </p>
        <p style={{ marginBottom: 10 }}>
          You may irreversibly delete your account at any time. Upon deletion of your account all your posts/ toots will also be deleted from all EUNOMIA nodes.
        </p>
        <p style={{ marginBottom: 10 }}>
          Data collected strictly for research purposes, such as participants’ feedback, will be pseudonymised and securely stored for up to five years after the end of the project.
        </p>
      </div>
      <div style={{ marginBottom: 10, marginTop: 20 }}>
        <h3>Participant data sharing</h3>
      </div>
      <div>
        We do not share participant personal data with third parties, however, if for any reason future data sharing is necessary we will seek your consent before doing so.
      </div>
      <div style={{ marginBottom: 10, marginTop: 20 }}>
        <h3>Your rights:</h3>
      </div>
      <p style={{ marginBottom: 10 }}>
        You have the following rights in relation to any of your personal data that we process. You can exercise your rights, explained below, by contacting by phone, in writing or emailing us at: compliance@greenwich.ac.uk.
      </p>
      <div style={{ marginBottom: 10 }}>
        <ul>
          <li>
            Right to withdraw consent– You can withdraw your consent that you have previously given to one or more specified purposes to process your personal data. This will not affect the lawfulness of any processing carried out before you withdraw your consent. It may mean we are not able to provide certain products or services to you and we will advise you if this is the case.
          </li>
          <li>
            Right of access– You can ask us to verify whether we are processing personal data about you, and if so, to have access to a copy of such data.
          </li>
          <li>
            Right to rectification and erasure– You can ask us to correct our records if you believe they contain incorrect or incomplete information about you or ask us to erase your personal data after you withdraw your consent to processing or when we no longer need it for the purpose it was originally collected.
          </li>
          <li>
            Right to restriction of processing– You can ask us to temporarily restrict our processing of your personal data if you contest the accuracy of your personal data, prefer to restrict its use rather than having us erase it, or need us to preserve it for you to establish, exercise, or defend a legal claim. A temporary restriction may apply while verifying whether we have overriding legitimate grounds to process it. You can ask us to inform you before we lift that temporary processing restriction.
          </li>
          <li>
            Right to data portability– In some circumstances, where you have provided personal data to us, you can ask us to transmit that personal data (in a structured, commonly used, and machine-readable format) directly to another company.
          </li>
          <li>
            Right to object – You can object to our use of your personal data for direct marketing purposes, including profiling or where processing has taken the form of automated decision making. However, we may need to keep some minimal information (e.g., email address) to comply with your request to cease marketing to you.
          </li>
          <li>
            Right to make a complaint to the UK Information Commissioner’s Office regarding any concerns you may have about our data handling practices.
          </li>
        </ul>
      </div>
      <p style={{ marginBottom: 10 }}>
        If you have any questions about this research or your prospective involvement in it, please contact:
      </p>
      <div style={{ marginBottom: 10 }}>
        <table>
          <thead>
            <tr>
              <td>
                Project coordinator
              </td>
              <td>
                Data Protection Officer
              </td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                Dr. George Loukas,<br />
                University of Greenwich<br />
                E-mail: G.Loukas@greenwich.ac.uk
              </td>
              <td>
                Peter Garrod,<br />
                University of Greenwich<br />
                E-mail: compliance@greenwich.ac.uk
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div
        className='container'
        style={{ color: '#737d99', display: 'flex', alignItems: 'center', marginBottom: 10 }}
      >
        <img src={euLogo} style={{ maxHeight: 40, marginRight: 10 }} alt='EU Logo' />
        <p>EUNOMIA project has received funding from the European Union’s H2020 research and innovation programme under
          the grant agreement No 825171</p>
      </div>
    </div>
  );
}
