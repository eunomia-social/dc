import * as faceApi from 'face-api.js';
import React from 'react';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import axios from 'axios';
import Column from '../../../../components/column';
import ColumnHeader from '../../../../components/column_header';
import { eunomiaIcon } from '../eunomia_icon';
import { withRouter } from 'react-router-dom';
import IconButton from '../../../../components/icon_button';
import {
  REFACE_BASE_URL,
  REFACE_TARGET_WIDTH,
  REFACE_TARGET_HEIGHT,
  REFACE_VIDEO_WIDTH,
  REFACE_VIDEO_HEIGHT,
} from '../../constants';
import { domainName, accessToken } from '../../../../initial_state';

@injectIntl
class RefaceUser extends React.PureComponent {

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    intl: PropTypes.object.isRequired,
    shouldUpdateScroll: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.run = this.run.bind(this);
    this.onPlay = this.onPlay.bind(this);
    this.runLoop = this.runLoop.bind(this);
    this.usePhoto = this.usePhoto.bind(this);
    this.clearPhoto = this.clearPhoto.bind(this);
    this.takePicture = this.takePicture.bind(this);
    this.getTargetBox = this.getTargetBox.bind(this);
    this.uploadPicture = this.uploadPicture.bind(this);
    this.drawTargetRect = this.drawTargetRect.bind(this);
    this.stopStreamedVideo = this.stopStreamedVideo.bind(this);
  }

  video = React.createRef();
  parent = React.createRef();
  canvas = null;
  photo = null;
  targetBox = null;
  photoParent = null;
  _interval = null;
  options = new faceApi.TinyFaceDetectorOptions({
    inputSize: 160,
    scoreThreshold: 0.6,
  });
  displaySize = { width: 0, height: 0 };
  state = { streaming: false, videoDevices: [], face: null, detection: null, facingMode: 'user' };

  componentDidMount() {
    const { attachment } = this.props.location;
    if (!attachment) {
      this.context.router.history.goBack();
    } else {
      this.run();
    }
  }

  stopStreamedVideo() {
    if (this.video.current) {
      const stream = this.video.current.srcObject;
      if (stream) {
        const tracks = stream.getTracks();
        tracks.forEach((track) => {
          track.stop();
        });
      }
      this.video.current.srcObject = null;
    }
  }

  componentWillUnmount() {
    if (this._interval) {
      clearInterval(this._interval);
    }
    this.stopStreamedVideo();
  }

  run() {
    // try {
    faceApi.nets.tinyFaceDetector.load('/models/').then(() => {
      navigator.mediaDevices.getUserMedia({ audio: false, video: { facingMode: this.state.facingMode, width: REFACE_VIDEO_WIDTH, height: REFACE_VIDEO_HEIGHT } }).then((mediaStream) => {
        this.mediaStream = mediaStream;
        this.video.current.srcObject = this.mediaStream;
        navigator.mediaDevices.enumerateDevices().then ((devices) => {
          const videoDevices = devices.filter(device => device.kind === 'videoinput');
          this.setState({ streaming: videoDevices.length > 0, videoDevices });
        }).catch((reason) => {
          console.warn(reason);
        });
      }).catch((reason) => {
        console.warn(reason);
      });
    }).catch((reason) => {
      console.warn(reason);
    });
  }
  getTargetBox() {
    if (this.canvas && this.displaySize) {
      const box = { x: 0, y: 0, width: Math.min(this.displaySize.width, REFACE_TARGET_WIDTH), height:  Math.min(this.displaySize.height, REFACE_TARGET_HEIGHT) };
      if (this.displaySize.width > REFACE_TARGET_WIDTH + 40) { // aim for portrait
        box.x = (this.displaySize.width - REFACE_TARGET_WIDTH) / 2;
        box.x += 20;
        box.width -= 40;
      } else {
        if (this.displaySize.width > REFACE_TARGET_WIDTH) {
          box.x = (this.displaySize.width - REFACE_TARGET_WIDTH) / 2;
        }
      }
      if (this.displaySize.height > REFACE_TARGET_HEIGHT) {
        box.y = (this.displaySize.height - REFACE_TARGET_HEIGHT) / 2;
      }
      return box;
    }
    return null;
  }
  acceptableBox(box) {
    const threshold = 40;
    if (this.video.current && this.state.streaming) {
      if (!this.targetBox) {
        this.targetBox = this.getTargetBox();
      }
      if (!this.targetBox) {
        return false;
      }
      return Math.abs(box.x - this.targetBox.x) < threshold &&
              Math.abs(box.y - this.targetBox.y -threshold) < threshold &&
              Math.abs(box.width - this.targetBox.width) < threshold &&
              Math.abs(box.height - this.targetBox.height + (threshold / 2)) < threshold;
    }
    return false;
  }
  drawTargetRect(boxColor='#FF0000') {
    if (this.canvas && this.displaySize) {
      if (!this.targetBox) {
        this.targetBox = this.getTargetBox();
      }
      this.canvas.getContext('2d').clearRect(0, 0, this.canvas.width, this.canvas.height);
      const drawBox = new faceApi.draw.DrawBox(this.targetBox, { label: '', boxColor });
      drawBox.draw(this.canvas);
    }
  }

  runLoop() {
    if (this.video.current) {
      if (this.state.streaming) {
        faceApi.detectSingleFace(this.video.current, this.options).then((detection) => {
          if (detection) {
            const face = faceApi.resizeResults(detection, this.displaySize);
            if (this.acceptableBox(face.box)) {
              this.drawTargetRect('#0000FF');
              this.setState({ face, detection });
            } else {
              this.drawTargetRect();
              this.setState({ face: null, detection: null });
            }
          } else {
            this.drawTargetRect();
            this.setState({ face: null, detection: null });
          }
        }).catch((reason) => {
          console.warn(reason);
          this.drawTargetRect();
          this.setState({ face: null, detection: null });
        });
      }
    }
  }

  onPlay () {
    this.canvas = faceApi.createCanvasFromMedia(this.video.current);
    const videoRect = this.video.current.getBoundingClientRect();
    this.displaySize = { width: videoRect.width, height: videoRect.height };
    this.canvas.style.marginTop = `-${this.displaySize.height}px`;
    this.parent.current.appendChild(this.canvas);
    faceApi.matchDimensions(this.canvas, this.displaySize);
    this._interval = setInterval(this.runLoop, 300);
  }

  uploadPicture(cb) {
    if (this.photo) {
      fetch(this.photo.src)
        .then(res => res.blob())
        .then((blob) => {
          const file = new File([blob], 'user.png', { type: 'image/png' });
          let formData = new FormData();
          formData.append('file', file);
          const url =  `${REFACE_BASE_URL}/reface/upload`;
          axios.post(url, formData, {
            headers: {
              'Content-Type': 'multipart/form-data',
              'X-SN-TOKEN': accessToken,
              'X-SN-URL': `https://${domainName}`,
            },
          }).then(({ data }) => {
            cb(data);
          }).catch((reason) => {
            console.warn(reason);
            cb(null);
          });
        }).catch((reason) => {
          console.warn(reason);
          cb(null);
        });
    } else {
      cb(null);
    }
  }

  usePhoto() {
    const { attachment } = this.props.location;
    if (!attachment || !attachment.name) {
      this.context.router.history.goBack();
    }
    this.uploadPicture((imageId) => {
      if (!imageId) {
        this.context.router.history.goBack();
      } else {
        const url =  `${REFACE_BASE_URL}/reface/swap?image=${imageId}&video=${attachment.name}`;
        axios.get(url, {
          headers: {
            'Content-Type': 'application/json',
            'X-SN-TOKEN': accessToken,
            'X-SN-URL': `https://${domainName}`,
          },
        }).then(({ data }) => {
          this.context.router.history.push('/reface/results', { task: data.job_id, attachment });
        }).catch((reason) => {
          console.warn(reason);
          this.context.router.history.goBack();
        });
      }
    });
  }

  clearPhoto() {
    if (this.photoParent) {
      if (this.photo) {
        this.photoParent.removeChild(this.photo);
        this.photo = null;
      }
      this.parent.current.removeChild(this.photoParent);
    }
    this.setState({ streaming: true });
  }

  takePicture() {
    if (this.state.streaming) {
      this.setState({ streaming: false });
      if (this.state.face !== null) {
        // clear current box
        this.canvas.getContext('2d').clearRect(0, 0, this.canvas.width, this.canvas.height);
        // parent element (size of canvas)
        this.photoParent = document.createElement('div');
        this.photoParent.style.marginTop = `-${this.displaySize.height}px`;
        this.photoParent.style.width = `${this.displaySize.width}px`;
        this.photoParent.style.height = `${this.displaySize.height}px`;
        this.photoParent.style.backgroundColor= 'black';
        this.photoParent.style['z-index'] = 10;
        let canvasDim = Math.min(this.video.current.videoWidth, this.video.current.videoHeight);
        // // new canvas with detected face on the center
        const _canvas = document.createElement('canvas');
        if (
          (this.video.current.videoWidth === this.video.current.videoHeight) &&
          (this.video.current.videoWidth === REFACE_VIDEO_WIDTH)
        ) {
          _canvas.width = canvasDim - 80;
          _canvas.height = canvasDim - 40;
          _canvas.getContext('2d').drawImage(
            this.video.current,
            (this.video.current.videoWidth - canvasDim + 80) / 2,
            (this.video.current.videoHeight - canvasDim + 40) / 2,
            canvasDim - 80,
            canvasDim - 40,
            0,
            0,
            canvasDim - 80,
            canvasDim - 40,
          );
        } else {
          _canvas.width = canvasDim;
          _canvas.height =  canvasDim;
          _canvas.getContext('2d').drawImage(
            this.video.current,
            (this.video.current.videoWidth - canvasDim) / 2,
            (this.video.current.videoHeight - canvasDim) / 2,
            canvasDim,
            canvasDim,
            0,
            0,
            canvasDim,
            canvasDim,
          );
        }
        this.photo = document.createElement('img');
        this.photo.src = _canvas.toDataURL('image/png');
        this.photo.style['max-width'] = `${this.displaySize.width}px`;
        this.photo.style['max-height'] = `${this.displaySize.height}px`;
        if (this.video.current.videoWidth !== this.video.current.videoHeight) {
          if (this.video.current.videoWidth > this.video.current.videoHeight) {
            this.photo.style.height = `${this.displaySize.height}px`;
            this.photo.style.marginLeft = `${(this.displaySize.width - this.displaySize.height) / 2}px`;
          } else {
            this.photo.style.width = `${this.displaySize.width}px`;
            this.photo.style.marginTop = `${(this.displaySize.height - this.displaySize.width) / 2}px`;
          }
        } else {
          if (this.displaySize.height > this.displaySize.width) {
            this.photo.style.height = `${this.displaySize.height}px`;
            this.photo.style.marginLeft = `${(this.displaySize.width - this.displaySize.height) / 2}px`;
          } else {
            this.photo.style.width = `${this.displaySize.width}px`;
            this.photo.style.marginTop = `${(this.displaySize.height - this.displaySize.width) / 2}px`;
          }
        }
        this.photo.style['z-index'] = 10;
        // add elements
        this.photoParent.appendChild(this.photo);
        this.parent.current.appendChild(this.photoParent);
      } else {
        this.clearPhoto();
      }
    } else {
      this.clearPhoto();
    }
  }

  render() {
    const { intl } = this.props;
    return (
      <Column bindToDocument ref={this.setRef} label='EUNOMIA Reface'>
        <ColumnHeader
          intl={intl}
          icon='home'
          customIcon={eunomiaIcon}
          active={false}
          title='EUNOMIA Reface'
          multiColumn={false}
          showBackButton
        />
        {/* eslint-disable-next-line react/jsx-no-bind */}
        <div>
          <div style={{ minHeight: '80vh' }}>
            <div ref={this.parent} className='reface-mobile'>
              <video
                ref={this.video}
                autoPlay
                muted
                playsInline
                onPlay={this.onPlay}
              />
            </div>
            <div className='reface_controls status__action-bar'>
              { this.canvas && this.state.videoDevices.length > 0 &&
                <IconButton
                  className='status__action-bar-button'
                  size={24}
                  title={this.state.streaming ? 'Take photo': 'Clear'}
                  icon={this.state.streaming ? 'camera-retro': 'eraser'}
                  onClick={this.takePicture}
                  disabled={!this.state.streaming ? false: !this.state.face}
                  style={{ marginLeft: 0, zIndex: 99 }}
                  pressed={false}
                />
              }
              {
                this.canvas && this.state.videoDevices.length > 0 &&
                <IconButton
                  className='status__action-bar-button'
                  size={24}
                  title={'Swap!'}
                  icon={'magic'}
                  onClick={this.usePhoto}
                  disabled={(this.state.streaming && !this.state.face) || ! this.photo}
                  style={{ marginRight: 0, zIndex: 99 }}
                  pressed={false}
                />
              }
            </div>
          </div>
        </div>
      </Column>
    );
  };

}

export default withRouter(RefaceUser);
