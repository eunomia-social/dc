import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import ImmutablePureComponent from 'react-immutable-pure-component';
import Icon from '../components/icon';
import { domainName } from '../initial_state';

const filename = url => url.split('/').pop().split('#')[0].split('?')[0];

export default class AttachmentList extends ImmutablePureComponent {

  static propTypes = {
    media: ImmutablePropTypes.list.isRequired,
    compact: PropTypes.bool,
  };

  handleAttachmentUrl = (attachment) => {
    let displayUrl = attachment.get('remote_url') || attachment.get('url');
    if (displayUrl && displayUrl.startsWith('http:') && displayUrl.includes(domainName) && window.location.protocol.startsWith('https')) {
      displayUrl = displayUrl.replace('http', 'https');
    }

    return displayUrl;
  }

  render () {
    const { media, compact } = this.props;

    if (compact) {
      return (
        <div className='attachment-list compact'>
          <ul className='attachment-list__list'>
            {media.map(attachment => {
              const displayUrl = this.handleAttachmentUrl(attachment);

              return (
                <li key={attachment.get('id')}>
                  <a href={displayUrl} target='_blank' rel='noopener noreferrer'><Icon id='link' /> {filename(displayUrl)}</a>
                </li>
              );
            })}
          </ul>
        </div>
      );
    }

    return (
      <div className='attachment-list'>
        <div className='attachment-list__icon'>
          <Icon id='link' />
        </div>

        <ul className='attachment-list__list'>
          {media.map(attachment => {
            const displayUrl = this.handleAttachmentUrl(attachment);

            return (
              <li key={attachment.get('id')}>
                <a href={displayUrl} target='_blank' rel='noopener noreferrer'>{filename(displayUrl)}</a>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }

}
