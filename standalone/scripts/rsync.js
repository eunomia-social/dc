/* eslint-disable no-console, import/no-extraneous-dependencies */
const { resolve } = require('path');
const fse = require('fs-extra');

const sedPrefix = process.platform === 'darwin' ? 'sed -i ""' : 'sed -i';

const sedManifestDomain = () => {
  const targetFile = resolve(__dirname, '..', 'public', 'eunomia', 'manifest.json');
  if (fse.existsSync(targetFile)) {
    // TODO: also check the syntax (non gnu-sed?) on OS X
    const command = `${sedPrefix} 's|"start_url": "/eunomia/",|"start_url": "https://${process.env.REMOTE_DOMAIN}/eunomia/",\\n  "url": "https://${process.env.REMOTE_DOMAIN}",|g' ${targetFile}`;
    try {
      require('child_process').execSync(command);
    } catch (err) {
      console.error(err);
      process.exit(1);
    }
  }
};

const runRsync = () => {
  const sshPort = process.env.RSYNC_PORT;
  const remoteUser = process.env.RSYNC_USER;
  const remoteDomain = process.env.REMOTE_DOMAIN;
  const remotePath = process.env.RSYNC_PATH;
  const src = resolve(__dirname, '..', 'public/');
  const dst = `${remoteUser}@${remoteDomain}:${remotePath}`;
  const command = `rsync -avz --chown=${remoteUser}:${remoteUser} ${src} --delete -e "ssh -p ${sshPort}" ${dst}`;
  try {
    const out = require('child_process').execSync(command).toString();
    console.log(out);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

const dotEnv = resolve(__dirname, '..', '.env');
if (fse.existsSync(dotEnv)) {
  require('dotenv').config({
    path: dotEnv,
  });
  sedManifestDomain();
  runRsync();
}

// rsync:
// to include/exclude files/dirs:
// const includeJs = '--include "/js/" --include "*.js" --include "*.json"';
// const includeCss = '--include "/css/" --include "*.css"';
// const includeMedia = '--include "/media/" --include "*.png" --include "*.jpg" --include "*.gif" --include "*.ttf"';
// const includes = `--include "*.html" ${includeJs} ${includeCss} ${includeMedia}`;
// const excludes = `--exclude "apple-touch-icon.png" --exclude "versions.json"`;
// require('child_process').execSync(`rsync -avz ${excludes} ${includes} --exclude "/**" --chown=${remoteUser}:${remoteUser} ${src} --delete ${src}/ ${dst}`).toString();
