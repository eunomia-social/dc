const CACHE = "pwabuilder-offline";

importScripts('/eunomia/workbox/workbox-sw.js');

self.addEventListener("message", (event) => {
  console.log(event);
  if (event.data && event.data.type === "SKIP_WAITING") {
    self.skipWaiting();
  }
});

workbox.routing.registerRoute(
  new RegExp('/*'),
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: CACHE
  })
);
