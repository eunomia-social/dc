// import * as registerPushNotifications from './actions/push_notifications';
import { setupBrowserNotifications } from './actions/notifications';
import { default as App, store } from './containers/eunomia';
import React from 'react';
import ReactDOM from 'react-dom';
import ready from './ready';

const perf = require('./performance');

function main() {
  perf.start('main()');

  if (window.history && history.replaceState) {
    const { pathname, search, hash } = window.location;
    const path = pathname + search + hash;
    if (!(/^\/eunomia($|\/)/).test(path)) {
      history.replaceState(null, document.title, `/eunomia${path}`);
    }
  }

  ready(() => {
    const mountNode = document.getElementById('root');
    const props = JSON.parse(mountNode.getAttribute('data-props'));
    ReactDOM.render(<App locale='en' {...props} />, mountNode);
    store.dispatch(setupBrowserNotifications());
    if (process.env.NODE_ENV === 'production') {
      // avoid offline in dev mode because it's harder to debug
      require('offline-plugin/runtime').install();
      // store.dispatch(registerPushNotifications.register());
    }
    perf.stop('main()');
  });
}

export default main;
