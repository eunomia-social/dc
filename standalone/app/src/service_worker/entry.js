import './web_push_notifications';

const fallbackName = 'offline';

const openWebCache = () =>
  new Promise((resolve) => {
    caches.keys().then((cacheNames) => {
      if (cacheNames.length > 0) {
        resolve(caches.open(cacheNames[0]));
      } else {
        resolve(caches.open(fallbackName));
      }
    }).catch(() => {
      resolve(caches.open(fallbackName));
    });
  });

// fetch the resource from the network
const fromNetwork = (request, timeout) =>
  new Promise((resolve, reject) => {
    const timeoutId = setTimeout(reject, timeout);
    // eslint-disable-next-line promise/catch-or-return
    fetch(request).then(response => {
      clearTimeout(timeoutId);
      resolve(response);
      update(request);
    }, reject);
  });

// fetch the resource from the browser cache
const fromCache = (request) =>
  openWebCache()
    .then((cache) =>
      cache
        .match(request)
        .then(matching => matching || caches.match('/eunomia/', { ignoreSearch: true })),
    );
// cache the current page to make it available for offline
const update = (request) =>
  openWebCache()
    .then(cache =>
      fetch(request).then(response => cache.put(request, response.clone())),
    );

self.addEventListener('fetch', (event) => {
  const url = new URL(event.request.url);
  if (event.request.method !== 'GET' || url.pathname.includes('/api/')) {
    return;
  }
  if (url.pathname === '/eunomia/logout') {
    event.respondWith(
      new Promise((resolve) => {
        fetch(event.request, { redirect: 'follow' }).then(() => {
          openWebCache().then(cache => cache.delete('/eunomia/')).catch(() => {
          }).finally(() => {
            indexedDB.deleteDatabase('eunomia');
          });
        }).catch(() => {}).finally(() => resolve(Response.redirect('/eunomia/')));
      }),
    );
  } else {
    if (url.pathname.startsWith('/eunomia/')) {
      event.respondWith(
        fromNetwork(event.request, 5000).catch(() => fromCache(event.request)),
      );
    }
  }
});
self.addEventListener('activate', function(event) {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('install', () => self.skipWaiting());

const fetchAndCacheUpdates = () => {
  // console.log('fetch updates');
};

self.addEventListener('periodicsync', event => {
  if (event.tag && event.tag === 'fetch-updates') {
    event.waitUntil(fetchAndCacheUpdates());
  }
});
