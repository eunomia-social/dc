import * as faceApi from 'face-api.js';
import React from 'react';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import Column from '../../../components/column';
import ColumnHeader from '../../../components/column_header';
import { eunomiaIcon } from './eunomia_icon';
import { inSinglePanel } from '../lib';

export default @injectIntl
class Reface extends React.PureComponent {

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    intl: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.run = this.run.bind(this);
    this.onPlay = this.onPlay.bind(this);
    this.runLoop = this.runLoop.bind(this);
    this.stopStreamedVideo = this.stopStreamedVideo.bind(this);
  }

  video = React.createRef();
  parent = React.createRef();
  options = new faceApi.TinyFaceDetectorOptions({
    inputSize: 512,
    scoreThreshold: 0.5,
  });
  displaySize = { width: 0, height: 0 };
  state = { error: '' };

  componentDidMount() {
    this.run();
  }

  stopStreamedVideo() {
    if (this.video.current) {
      const stream = this.video.current.srcObject;
      if (stream) {
        const tracks = stream.getTracks();

        tracks.forEach((track) => {
          track.stop();
        });
      }
      this.video.current.srcObject = null;
    }
  }

  componentWillUnmount() {
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.stopStreamedVideo();
  }

  run() {
    // try {
    faceApi.nets.tinyFaceDetector.load('/models/').then(() => {
      navigator.mediaDevices.getUserMedia({ audio: false, video: { facingMode: 'user' } }).then((mediaStream) => {
        this.mediaStream = mediaStream;
        this.video.current.srcObject = this.mediaStream;
      }).catch((reason) => {
        console.warn(reason);
      });
    }).catch((reason) => {
      console.warn(reason);
    });
  }

  runLoop() {
    if (this.video.current) {
      faceApi.detectSingleFace(this.video.current, this.options).then(detection => {
        if (detection) {
          const resizedDetection = faceApi.resizeResults(detection, this.displaySize);
          this.canvas.getContext('2d').clearRect(0, 0, this.canvas.width, this.canvas.height);
          const drawBox = new faceApi.draw.DrawBox(resizedDetection.box, { label: '' });
          drawBox.draw(this.canvas);
        } else {
          this.canvas.getContext('2d').clearRect(0, 0, this.canvas.width, this.canvas.height);
        }
      }).catch((reason) => {
        console.warn(reason);
      });
    }
  }

  onPlay () {
    this.canvas = faceApi.createCanvasFromMedia(this.video.current);
    const videoRect = this.video.current.getBoundingClientRect();
    this.displaySize = { width: videoRect.width, height: videoRect.height };
    this.canvas.style.marginTop = `-${this.displaySize.height}px`;
    this.parent.current.appendChild(this.canvas);
    faceApi.matchDimensions(this.canvas, this.displaySize);
    this.runInterval = setInterval(this.runLoop, 100);
  }

  render() {
    const { intl } = this.props;
    return (
      <Column bindToDocument ref={this.setRef} label='EUNOMIA Reface'>
        <ColumnHeader
          intl={intl}
          icon='home'
          customIcon={eunomiaIcon}
          active={false}
          title='EUNOMIA Reface'
          multiColumn={false}
          showBackButton
        />
        <div className={inSinglePanel() ? 'reface-mobile': 'reface'} ref={this.parent}>
          <video
            ref={this.video}
            autoPlay
            muted
            playsInline
            onPlay={this.onPlay}
          />
          <div className='error'>{this.state.error}</div>
        </div>
      </Column>
    );
  };

}
