/* eslint-disable import/no-extraneous-dependencies */
const { resolve } = require('path');
const fse = require('fs-extra');

const dotEnv = resolve(__dirname, '..', '.env');
const provisionProfile = resolve(__dirname, '..', 'embedded.provisionprofile');
if (fse.existsSync(dotEnv)) {
  require('dotenv').config({
    path: resolve(__dirname, '..', '.env'),
  });
  const { notarize } = require('electron-notarize');

  exports.default = async function notarizing(context) {
    const { electronPlatformName, appOutDir } = context;
    if (electronPlatformName !== 'darwin' || !(fse.existsSync(provisionProfile))) {
      return;
    }

    const appName = context.packager.appInfo.productFilename;

    await notarize({
      appBundleId: 'gr.uniwa.eee.consert.dc',
      appPath: `${appOutDir}/${appName}.app`,
      appleId: process.env.APPLEID,
      appleIdPassword: process.env.APPLEIDPASS,
      ascProvider: process.env.TEAMID,
    });
  };
}
