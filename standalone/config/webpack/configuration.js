const { resolve, join } = require('path');
const { env } = require('process');

const settings = {
  source_path: 'app',
  source_entry_path: 'packs',
  public_root_path: 'public',
  public_output_path: 'eunomia/static',
  cache_path: 'tmp/cache/webpacker',
  check_yarn_integrity: false,
  webpack_compile_output: false,
  resolved_paths: [],
  cache_manifest: false,
  extract_css: true,
  static_assets_extensions: [
    '.jpg',  '.gif',
    '.jpeg', '.png',
    '.tiff', '.ico',
    '.svg',  '.eot',
    '.otf',  '.ttf',
    '.woff', '.woff2',
  ],
  extensions: [
    '.mjs',         '.js',
    '.sass',        '.scss',
    '.css',         '.module.sass',
    '.module.scss', '.module.css',
    '.png',         '.svg',
    '.gif',         '.jpeg',
    '.jpg',
  ],
  compile: true,
  dev_server: {
    https: false,
    host: '0.0.0.0',
    port: 3035,
    public: '0.0.0.0:3035',
    content_base: 'public/eunomia',
    hmr: false,
    inline: true,
    overlay: true,
    compress: true,
    disable_host_check: true,
    use_local_ip: false,
    quiet: false,
    headers: { 'Access-Control-Allow-Origin': '*' },
    watch_options: { ignored: '**/node_modules/**' },
  },
};


const themes = {
  default: 'styles/application.scss',
  'eunomia-light': 'styles/eunomia-light.scss',
};
const output = {
  path: join(resolve('public', 'eunomia'), 'static'),
  publicPath: '/eunomia/static/',
};

module.exports = {
  settings,
  themes,
  env: {
    NODE_ENV: env.NODE_ENV,
    PUBLIC_OUTPUT_PATH: '/eunomia/static/',
  },
  output,
};
